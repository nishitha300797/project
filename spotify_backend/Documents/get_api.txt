api1:
path: "/user_data"
method: GET
schema :{
    id: string,
    name: string,
    image_url:string
}
Description: when we are calling api with "/user_data" path we get user data

api2:
path: "/artist_details"
method : GET
schema :{
    id: string,
    name: string,
    image_url: string,
    monthly_listener_count:number
}
Description: when we are calling api with "/artist_details" path we are getting the object with respected data

api3:
path: "/latest_release"
method: GET
schema: {
    id:string,
    name:string,
    date:date,
    image_url:string
}
Description: when we are calling api with "/latest_release" path we are getting the object with respected data

api4:
path: "/popular_songs"
method: GET
schema: {
    id: string,
    name:string,
    image_url: string,
    is_check: boolean,
    listener_count: number
}
Description: when we are calling api with "/popular_songs" path we are getting the object with respected data

api5:
path: "/related_artist"
method: GET
schema: {
    id: string,
    name: string,
    image_url: string
}
Description: when we are calling api with "/related_artist" path we are getting the object with respected data

api6:
path: "/album_data"
method: GET
schema: {
    id: string,
    name: string,
    image_url: string,
    year_of_release: number
}
Description: when we are calling api with "/album_data" path we are getting the object with respected data

api7:
path: "/playlist/album_id"
method: GET
schema: {
    id: string,
    song_name: string,
    image_url: string,
    duration: string,
    album_id: string,
    song_url: string
}
Description: when we are calling api with "/playlist/album_id" path we are getting the object with respected data

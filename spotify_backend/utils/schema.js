// importing mangoose from mangoose
import mongoose from "mongoose";

/*
    creating a schema for user_register_data
    in that withrespected datatypes
*/
const USER_REGISTER_SCHEMA = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  //re_enter_password: String,
  email: String,
  phone_number: Number,
  security_question: String,
  user_role: String // admin, user, premiummonthly, premiumweekly, premiumdaily, premiumannual, artist, owner, adder
});

// /*
//     creating a schema for admin_register_data
//     in that withrespected datatypes
// */
// const ADMIN_REGISTER_SCHEMA = new mongoose.Schema({
//   name: String,
//   username: String,
//   password: String,
//   //re_enter_password: String,
//   email: String,
//   phone_number: Number,
//   security_question: String,
// });

/* 
    creating a schema for the user_login_data
    withrespected datatypes
// */
// const USER_LOGIN_SCHEMA = new mongoose.Schema({
//   username: String,
//   password: String,
//   jwt_token: String,
//   logon_time: Date,
// });

/* 
    creating a schema for the admin_login_data
    withrespected datatypes
*/
// const ADMIN_LOGIN_SCHEMA = new mongoose.Schema({
//   username: String,
//   password: String,
//   jwt_token: String,
//   logon_time: Date,
// });


const Song = new mongoose.Schema({
  name: String,
  duration: String, 
  image: {type: String}, // optional
  album: { Album }, // optionals
  artist: { Artist }
})

const ArtistDetails = new mongoose.Schema({ // Artist
  name:String,
  image:String,
  monthly_listener:String,
  // #followers, 
  songs: [],
  related_artists: [ Artist ]
})

const Popular = new mongoose.Schema({
  image:String,
  isCheck :String,
  songName:String,
  listenercount:Number
})


const AlbumData = new mongoose.Schema({
  image:String,
  year:String,
  name:String,
  birth: DateTime
})


export {
  USER_REGISTER_SCHEMA,
  USER_LOGIN_SCHEMA,
  ADMIN_REGISTER_SCHEMA,
  ADMIN_LOGIN_SCHEMA,
  ArtistDetails,
  LatestRelease,
  Popular,
  RelatedArtist,
  AlbumData,
  AlbumSong
};

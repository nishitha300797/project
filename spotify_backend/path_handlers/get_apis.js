
/** 
 * 
*/
const get_user_data_handler = (request,response)=>{
  // client_key is the jwt token only logedin user are access the restaurent data
  const {client_key} = request.headers
  // fetching the user login data for validating the client_key
  USER_LOGIN_SCHEMA.find({jwt_token:client_key},(err,data)=>{
    // if any error in the fetching restaurant data it prints the error
    if(err){
      console.log(err)
    // else it goes through the below block
    }else{
      // if data count is 0 it means there is no loged in user
      if(data.length === 0){
        response.send("your not a login user")
      }
      // 
      else{
        response.send("yor are existing user");
      }
    }
  })


}

/**
 * 
 */
 const get_artist_details_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const popular_songs_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const related_artist_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const latest_release_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const album_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const playlist_data_handler = (request,response)=>{
    response.send("hello")
}

// exporting all handler_functions 
export{
       get_user_data_handler,
       get_artist_details_handler,
       playlist_data_handler,
       popular_songs_handler,
       album_data_handler,
       latest_release_data_handler,
       related_artist_data_handler
      }
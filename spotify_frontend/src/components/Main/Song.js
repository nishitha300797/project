// importing the app.css for styling
import '../../App.css'

import {Component} from 'react'

class Song extends Component{
    render(){
        const{each} = this.props
        return(
            <div className="track">
                <div className="track__number">{each.id}</div>
                <div className="track__added">
                    <i className="ion-checkmark-round added" />
                </div>
                <div className="track__title">{each.song_name}</div>
                <div className="track__length">{each.song_time}</div>
                <div className="track__popularity">
                    <i className="ion-arrow-graph-up-right" />
                </div>
            </div>
        )
    }
}
export default Song
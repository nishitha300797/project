import { Component } from "react";
import Header from "../Common/Header";
import Navigation from "../Common/leftPanel/Navigation";
import AlbumHeader from "./AlbumHeader";
import ArtistInfo from "./ArtistInfo";
import ArtistNavigation from "./ArtistNavigation";
import LatestRelease from "./LatestRelease";
import Popular from "./Popular";
import Related from "./Related";
import Album from "./Album";
import SongList from "./SongList";
import RelatedArtistList from "./RelatedArtistList";
import popular from '../../data'
import related_artist from '../../data'
import header from '../../data'
import song_info from '../../data'
import main_panel from '../../data'
import Body from "../body";

class MainComponent extends Component{
    render(){
        return(
            <>
      <Header header={header}/>
      <section className="content">
      <Navigation/>
      <div className="content__middle">
      <div className="artist is-verified">
        <div className="artist__header">
          <ArtistInfo  artist_data = {main_panel}/>
          <ArtistNavigation/>
        </div>
        <div className="artist__content">
          <div className="tab-content">
            <div
              role="tabpanel"
              className="tab-pane active"
              id="artist-overview"
            >
              <div className="overview">
                <div className="overview__artist">
                  <LatestRelease/>
                  <Popular popularlist={popular}/>
                </div>
                <Related RelatedArtist={related_artist}/>
                <div className="overview__albums">
                  <AlbumHeader/>
                  <div className="album">
                  <Album/>
                   <SongList song_info={song_info}/>
                  </div>
                </div>
              </div>
            </div>
            <RelatedArtistList related_artist={related_artist} />
          </div>
        </div>
      </div>
      </div>
      </section>
  </>
        )
    }
}

export default MainComponent
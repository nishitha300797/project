import {Component} from 'react'

class Artists extends Component{
   render(){
     const {artists} = this.props
     return(
      <a href="#" className="related-artist">
      <span className="related-artist__img">
        <img
          src={artists.image}
          alt={artists.name}
        />
      </span>
      <span className="related-artist__name">{artists.name}</span>
    </a>
     )
   }
}

export default Artists
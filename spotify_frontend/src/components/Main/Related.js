import { Component } from "react";
import Artists from "./Artists";

class Related extends Component {
  render() {
    const { related_artist } = this.props.RelatedArtist;
    return (
      <>
        <div className="overview__related">
          <div className="section-title">Related Artists</div>
          <div className="related-artists">
            {related_artist.map((eachartist) => (
              <Artists artists={eachartist} />
            ))}
          </div>
        </div>
      </>
    );
  }
}

export default Related;

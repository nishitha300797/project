import { Component } from "react";

class AlbumHeader extends Component{
    render(){
        return(
            <div className="overview__albums__head">
                <span className="section-title">Albums</span>
                <span className="view-type">
                      <i className="fa fa-list list active" />
                      <i className="fa fa-th-large card" />
                </span>
            </div>
        )
    }
}

export default AlbumHeader
import {Component} from 'react'


class Main extends Component{
  render(){
    return(
      <div className="navigation__list">
          <div
            className="navigation__list__header"
            role="button"
            data-toggle="collapse"
            href="#main"
            aria-expanded="true"
            aria-controls="main"
          >
            Main
          </div>
          <div className="collapse in" id="main">
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-browsers" />
              <span>Browse</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-person-stalker" />
              <span>Activity</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-radio-waves" />
              <span>Radio</span>
            </a>
          </div>
        </div>
    )
  }
}

export default Main
import {Component} from "react"


class Playlist extends Component{
  render(){
    return(
      <div className="navigation__list">
          <div
            className="navigation__list__header"
            role="button"
            data-toggle="collapse"
            href="#playlists"
            aria-expanded="true"
            aria-controls="playlists"
          >
            Playlists
          </div>
          <div className="collapse in" id="playlists">
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Doo Wop</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Pop Classics</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Love $ongs</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Hipster</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>New Music Friday</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Techno Poppers</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Summer Soothers</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Hard Rap</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Pop Rap</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>5 Stars</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Dope Dancin</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Sleep</span>
            </a>
          </div>
        </div>
    )
  }
}

export default Playlist
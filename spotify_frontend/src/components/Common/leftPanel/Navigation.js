import { Component } from "react";

import Main from "./Main";
import Music from "./Music";
import Playlist from "./Playlists";

class Navigation extends Component {
  render() {
    return (
      <div className="content__left">
        <section className="navigation">
          <Main />
          <Music />
          <Playlist />
        </section>
        <section className="playlist">
          <a href="#">
            <i className="ion-ios-plus-outline" />
            New Playlist
          </a>
        </section>
        <section className="playing">
          <div className="playing__art">
            <img
              src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/7022/cputh.jpg"
              alt="Album Art"
            />
          </div>
          <div className="playing__song">
            <a className="playing__song__name">Some Type of Love</a>
            <a className="playing__song__artist">Charlie Puth</a>
          </div>
          <div className="playing__add">
            <i className="ion-checkmark" />
          </div>
        </section>
      </div>
    );
  }
}

export default Navigation;

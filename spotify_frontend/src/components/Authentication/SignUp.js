import { Component } from "react";
import {Redirect} from 'react-router-dom'
import './SignUp.css'

class SignUp extends Component{
    state = {
      name:"",
      username:"",
      password:"",
      email:"",
      phone_no:"",
      re_enter_password:"",
      security_question:"",
      error_msg:""
    }

    changeName = (event)=>{
      this.setState({name:event.target.value})
    }

    changeUserName=(event)=>{
      this.setState({username:event.target.value})
    }

    changePassword = (event)=>{
      this.setState({password:event.target.value})
    }

    changeReEnterPassword = (event)=>{
      this.setState({re_enter_password:event.target.value})
    }

    changeEmail = (event)=>{
      this.setState({email:event.target.value})
    }

    changePhoneNo = (event)=>{
      this.setState({phone_no:event.target.value})
    }

    changeSecurityQuestion = (event)=>{
      this.setState({security_question:event.target.value})
    }

    redirectToSignin = ()=>{
      const{history} = this.props
      history.replace("/login")
    }

    submitingSignup = async(event)=>{
      event.preventDefault()
      const url = "http://localhost:3001/register/user"
      const{name,username,password,re_enter_password,email,phone_no,security_question} = this.state
      const userDetails = {
        name,
        username,
        password,
        re_enter_password,
        email,
        phone_no,
        security_question
      }
      const option = {
        method:"POST",
        body: JSON.stringify(userDetails),
        headers:{
          "Content-Type" :"application/json"
        }
      }
      const response = await fetch(url,option)
      console.log(response)
    }


    render(){
      const{name,username,password,re_enter_password,email,phone_no,security_question} = this.state
        return(
        <section className="login">
        <div className="container">
          <h3 className="title">Sign up</h3>
          <div className="social-login">
            <a href="https://www.facebook.com"><button className="button123"><i className="fab fa-facebook-f" /> Facebook </button></a>
            <a href="https://www.google.com"><button className="button123"><i className="fab fa-google" /> Google </button></a>
          </div>
          <p className="separator"><span>&nbsp;</span>Or<span>&nbsp;</span></p>
          <form onSubmit={this.submitingSignup}>
            <div className="form-group">
              <label htmlFor="name" />
              <input type="text" id="fullname" placeholder="Name" value={name} onChange={this.changeName} />          
            </div>
            <div className="form-group">
              <label htmlFor="username" />
              <input type="text" id="username" placeholder="User Name" value={username} onChange={this.changeUserName} />          
            </div>
            <div className="form-group">
              <label htmlFor="login as" />
              <input type="password" id="password" placeholder="Password" value={password} onChange={this.changePassword} />          
            </div> 
            <div className="form-group">
              <label htmlFor="age" />
              <input type="password" id="reEnterPassword" placeholder="re-enter-password" value={re_enter_password} onChange={this.changeReEnterPassword} />          
            </div>
            <div className="form-group">
              <label htmlFor="Email" />
              <input type="Email" id="Email" placeholder="Email" value={email} onChange={this.changeEmail} />          
            </div>
            <div className="form-group">
              <label htmlFor="password" />
              <input type="text" id="phone no" placeholder="Phone No" value={phone_no} onChange={this.changePhoneNo} />          
            </div>
            <div className="form-group">
              <label htmlFor="password" />
              <input type="text" id="security" placeholder="Security Question" value={security_question} onChange={this.changeSecurityQuestion} />          
            </div>
            <button type="submit" className="button123">Sign up</button>
          </form>
          <p className="additional-act">Already have an account? <span onClick={this.redirectToSignin}> Sign in </span></p>
        </div>
      </section>
        )
    }
}

export default SignUp